#!/bin/bash

# Bail out if anything goes wrong
set -e

# Get path to blivet
blivet_path=$(find /usr/ -path */site-packages | tr '\n' ':')

# Run upstream unit tests
cd ./source/
PYTHONPATH=$blivet_path python3 tests/run_tests.py
